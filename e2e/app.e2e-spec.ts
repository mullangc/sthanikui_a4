import { SthanikUIPage } from './app.po';

describe('sthanik-ui App', () => {
  let page: SthanikUIPage;

  beforeEach(() => {
    page = new SthanikUIPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
